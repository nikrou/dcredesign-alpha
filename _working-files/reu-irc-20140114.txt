[20:57:29] <franckpaul> 'soir les gens
[20:57:37] <nikrou1> Salut Franck
[20:58:30] <Dsls> 'lo
[20:59:14] <JcDenis> présent
[20:59:26] <franckpaul> \o/ y'a du monde, c'est cool !
[21:00:08] <Dsls> bon, on (re)casse tout ? :D
[21:00:18] <franckpaul> pour la 2.6.2 ? oui bien sûr
[21:00:22] <franckpaul> et pis après t'es mort
[21:00:27] <Dsls> chiche
[21:00:47] <franckpaul> Dsls: t'as un truc en tête ?
[21:00:55] <kozlika> coucou toulmonde
[21:01:07] <Dsls> franckpaul: je peux trouver s'il faut
[21:01:17] <franckpaul> un candidat pour le CR ?
[21:01:27] <Dsls> genre on met de l'openid connect dans auth.php
[21:01:49] <JcDenis> Dsls auth2
[21:01:54] <franckpaul> ah ué, ça peut, ça fait longtemps que ça me titille d'ouvrir un peu l'auth
[21:02:05] <Dsls> c'est openid connect qui a le vent en poupe en ce moment
[21:02:09] <franckpaul> 'soir lipki 
[21:02:11] <Dsls> cf gartner
[21:02:19] <lipki> bonsoir
[21:02:23] <kozlika> hello lipki !
[21:02:32] <franckpaul> dans 1 minute je désigne un candidat volontaire d'office
[21:02:52] <lipki> je viens a peine de voir l'heure, je suis là de justesse :)
[21:03:14] <kozlika> :)
[21:03:37] <Dsls> http://openid.net/connect/
[21:03:49] <franckpaul> =========== DEBUT ============
[21:04:25] <franckpaul> 1. 2.6.2 rapidement (y'a pas grand chose à dire de plus que dans mon mail de ce midi)
[21:04:59] <franckpaul> JcDenis ? Tu t'occupes du ticket d'ici la fin de la semaine ?
[21:05:22] <franckpaul> (comme ça je prépare le reste pendant ce temps et vendredi ou samedi on la sort)
[21:05:33] <franckpaul> 'soir llu_ 
[21:05:34] <JcDenis> je fais tout pour
[21:05:37] <franckpaul> super
[21:05:39] <kozlika> plop llu_ !
[21:05:42] <llu_> bonsoir !
[21:05:46] <llu_> j'avais peur d'être en retard
[21:06:08] <franckpaul> 2. Billet Dsls -> d'ici fin semaine prochaine si j'ai bien compris ?
[21:06:20] <Dsls> yep
[21:06:31] <Dsls> je ferai un balayage sur la 2.7 et la branche twig
[21:06:34] <franckpaul> (en plus ça va me servir de doc pour factoriser le jeu de template currywurst)
[21:06:51] <franckpaul> Dsls: essplique ce que tu entends par "balayage"
[21:06:52] <llu_> (ah bah zut, oui je suis en retard :s)
[21:07:04] <franckpaul> (spa grave llu_)
[21:07:14] <franckpaul> (t'es moins en retard que Jean-Michel)
[21:07:15] <Jean-Michel> Bonsoir
[21:07:31] <franckpaul> et voilà le dernier attendu qui hérite de la rédaction du CR \o/
[21:07:40] <franckpaul> 'soir Jean-Michel :-D
[21:08:12] <Jean-Michel> tut tut tut&
[21:08:36] <Jean-Michel> Premier sujet?
[21:08:43] <franckpaul> Jean-Michel: c'est « Chef ! Oui chef ! » qu'il faut répondre :-)
[21:09:10] <franckpaul> Dsls: balayage ?
[21:09:28] <Dsls> franckpaul: un tour d'horizon ce ce qui arrivera
[21:09:37] <Dsls> (pardon j'étais parti me faire un thé)
[21:09:41] <franckpaul> (PAN)
[21:09:46] <Dsls> donc pour la 2.7, l'héritage de templates
[21:09:52] <Dsls> et ce qui est prévu avec twig
[21:10:15] <franckpaul> ok
[21:10:34] <Dsls> si c'est trop long, je ferai 2 billets
[21:10:54] <franckpaul> Dsls: yep, vaut mieux plusieurs pas très gros, sinon les lecteurs zappent
[21:11:20] <Dsls> toutafé
[21:11:27] <franckpaul> ok
[21:11:37] <nikrou1> ballayage derrière les oreilles ?
[21:11:40] <franckpaul> 3. editeurs legacy and co
[21:11:43] <franckpaul> nikrou1: :-D
[21:12:01] <nikrou1> oui nikrou sans numéro s'est connecté avant moi ! :-)
[21:12:12] <nikrou> voleur !
[21:13:05] <franckpaul> t'en es où de tes dev pour l'éditeur ?
[21:13:28] <nikrou> au même point qu'au mois de décembre ! :-(
[21:13:42] <nikrou> Il y a des trucs qui fonctionnent d'autres pas
[21:13:43] <franckpaul> c'est à dire ? (c'est pour qu'on se rafraichisse la mémoire)
[21:13:58] <franckpaul> des points de blocage ?
[21:14:01] <lipki> y'a des trucs visible ?
[21:14:05] <nikrou> Je peux en faire un vrai plugin que j'essaie de mettre à jour pour que vous puissiez jouer si vous voulez
[21:14:16] <franckpaul> ok ça serait bien oui
[21:14:30] <nikrou> Je dois avouer que je ne me rappelle plus trop.
[21:14:34] <Jean-Michel> y'a de la contrebande de plugins ? :)
[21:14:40] <nikrou> Je parle des points de blocage
[21:14:47] <franckpaul> spa grave, tu replonges dedans dans la semaine et on check lundi prochain ?
[21:15:07] <lipki> un regard neuf aidera sans doute
[21:15:12] <nikrou> ça marche
[21:15:15] <lipki> voir 19 regard neuf
[21:15:16] <franckpaul> super, merci
[21:15:17] <nikrou> oui lipki
[21:15:33] <Dsls> nikrou: faudra regarder les tickets #1904 et #1905
[21:15:39] <lipki> 19 je suis optimiste
[21:16:28] <franckpaul> Dsls: on fera un tour des tickets de la 2.7 lundi prochain (j'aurais fait une revue d'ici là)
[21:16:57] <franckpaul> (donc au programme lundi prochain : 2.7 et uniquement 2.7, y'aura pas mal de choses à dire/discuter)
[21:17:21] <franckpaul> (parce que je voudrais aussi qu'on torde le cou au load des js, jquery and co)
[21:17:53] <kozlika> owi owi
[21:18:21] <nikrou> Dsls : on doit pouvoir tester qu'un behavior est utilisé non ?
[21:18:48] <Dsls> nikrou: pas à ma connaissance. Du moins, pas dans l'implémentation actuelle
[21:19:05] <nikrou> ah bon ?
[21:19:21] <franckpaul> euh& sysInfo est capable de lister tous les behaviours admin et public
[21:19:27] <franckpaul> ou alors on parle pas de la même chose
[21:20:30] <Dsls> j'ai pas dit que c'était pas faisable :) Le plus propre serait de définir une méthode dcCore::isBehaviorImplemented("name")
[21:20:37] <lipki> comment savoir qu'un behavior est utiliser, ils ne sont pas tous dans l'admin
[21:21:03] <franckpaul> lipki: tu parles des behaviours publics ?
[21:21:26] <lipki> de tous et vous ?
[21:21:44] <franckpaul> lipki: tu as joué avec sysInfo ou jamais ?
[21:22:05] <lipki> oui une fois, j'ai crus que c'était une liste faite a la main
[21:22:10] <franckpaul> tu rigoles
[21:22:11] <Dsls> d'ailleurs, $toto=$core->callBehavior("blabla") n'est pas vraiment correct, vu qu'on peut avoir plusieurs appels
[21:22:16] <franckpaul> j'suis trop faignant pour ça
[21:22:24] <lipki> copier de la doc
[21:22:39] <Dsls> il vaut mieux un $core->callBehavior("blabla",array(&$toto))
[21:22:45] <franckpaul> sysInfo fourni la liste des behaviours admin et un lien pour lister les publics via un URL Handler
[21:22:46] <Dsls> ou jouer avec $toto comme objet
[21:23:33] <franckpaul> 'soir Romy tetue 
[21:23:50] <tetue> bsoir :)
[21:24:25] <franckpaul> tu tombes à pic, on allait causer de ton atelier
[21:24:50] <franckpaul> 4. l'atelier syntaxe/édition/wiki/markdown-c-est-cool/dotclear-c-mieux 
[21:25:19] <franckpaul> donc on a, pour l'instant, 3 candidats rédacteurs prêts à venir causer d'édition
[21:25:40] <franckpaul> llu_ et Jean-Michel ici présents et Sylvain (pas là ce soir)
[21:25:54] <tetue> \o/
[21:25:57] <franckpaul> tetue: tu veux nous en dire deux mots ?
[21:26:08] <tetue> yep
[21:26:19] <tetue> il faudrait que je formalise, via mail par exemple
[21:26:22] <tetue> mais voici déjà :
[21:26:54] <tetue> il s'agit, après avoir rapidement vu la problématique de rédaction sur le web, d'étudier, ensemble, les syntaxes de rédaction
[21:27:55] <franckpaul> tu as un objectif final, ou c'est simplement une revue/comparaison ?
[21:27:55] <tetue> on étudie donc, par l'exemple, les syntaxes, pour les classer/critiquer ensemble
[21:28:27] <tetue> le but nest pas de décider, mais de nourrir la réflexion
[21:28:35] <tetue> avant de décider laquelle choisir
[21:29:04] <franckpaul> & et compléter/forker ?
[21:29:12] <lipki> textile
[21:29:20] <tetue> oui, on va jusqu'à cette éventualité là
[21:29:22] <franckpaul> ok
[21:29:38] <franckpaul> lipki: oui ça peut aussi être textile, dokuwiki, wikimedia, &
[21:29:41] <lipki> hein quoi faut discuter ... a bon !
[21:29:43] <tetue> je me positionne de façon neutre  je n'ai pas d'avis
[21:29:50] <franckpaul> yep
[21:30:19] <tetue> mais j'apporte le brainstrom qui va aider à décider (après, ça se fait en fonction de l'historique projet, des forces de dev, etc.)
[21:30:44] <franckpaul> ok
[21:30:45] <tetue> lipki: oui, y'a textile dans le lot :)
[21:31:15] <franckpaul> tetue: question : ta "base" c'est :
[21:31:22] <franckpaul> 1. une syntaxe wiki particulière
[21:31:28] <franckpaul> 2. le markup HTML 5
[21:31:29] <franckpaul> .
[21:31:30] <franckpaul> ?
[21:31:43] <tetue> ça fait partie des critères de choix à discuter :)
[21:31:57] <franckpaul> donc pas d'a priori ni de direction de départ
[21:32:02] <tetue> l'élaboration des critères est collective (avec suggestions)
[21:32:05] <franckpaul> ok
[21:32:13] <franckpaul> ça va être cool cet atelier !
[21:32:16] <tetue> oui
[21:32:29] <tetue> j'espère :)
[21:33:29] <franckpaul> merci tetue pour le p'tit brief
[21:33:35] <franckpaul> des questions les gens ?
[21:33:38] <tetue> on peut être 10 ou 20
[21:33:49] <tetue> c'est bien de brainstormer à plusieurs
[21:34:17] <llu_> euh, j'ai pas tout compris
[21:34:33] <tetue> llu_: pas grave, viens, tu verras, c'est cool :)
[21:34:49] <franckpaul> romy tu nous ferais pas un petit mail dans la semaine pour causer de ça ?
[21:34:52] <tetue> grosso modo, on va parler de comment t'aimerais rédiger idéalement
[21:34:55] <llu_> ouais, je vous fais confiance
[21:34:59] <tetue> oui franckpaul 
[21:35:03] <franckpaul> super :-)
[21:35:12] <tetue> c'est moi qui suit ravie :)
[21:36:07] <franckpaul> d'autres questions (ou remarques) ?
[21:36:31] <JcDenis> nop
[21:36:32] <kozlika> pas pour l'instant, peut être après le brief de têtue 
[21:36:37] <Dsls> pas d'avis de mon coté
[21:36:47] <franckpaul> ok
[21:36:55] <tetue> franckpaul: pourquoi, c'est numéroté « 4 » ?
[21:37:00] <franckpaul> 5. Questions diverses
[21:37:22] <JcDenis> 5 => Pourquoi mon Russian fuit ce soir ?
[21:37:42] <Dsls> 5. Pourquoi il n'y a plus de mammouths ?
[21:37:44] <tetue> c'est l'ordre du jour ?
[21:37:47] <franckpaul> tetue: ordre du jour de la réunion IRC code (cf mon mail de ce midi)
[21:37:58] <franckpaul> Dsls: y'en a plus ?
[21:38:01] <franckpaul> merdre alors !
[21:38:15] <Dsls> :) Parce qu'il n'y a plus de pappouths (c) Zeubeubeu
[21:38:19] <lipki> y'en a encore en sorbet
[21:38:21] <franckpaul> pourtant j'ai vu un reportage qui s'appelait "l'âge de glace"
[21:38:31] <lipki> mais faut les manger avant qu'il fondent
[21:38:38] <nikrou> mince quelqu'un doit lui dire ...
[21:39:00] <nikrou> qui lui dit en le préparant que ce n'était pas un vrai reportage ! :-)
[21:39:23] <lipki> C'était pas un reportage !!!
[21:39:41] <franckpaul> lipki: arrête, on est pas le 1er avril, c'est un peu gros comme mensonge
[21:39:44] <lipki> Et t'annonce ça comme ça
[21:40:11] <nikrou> et le père noel , et la petite souris, et les cloches ....
[21:40:22] <lipki> ch'uis un trop bon acteur par écrit
[21:40:29] <lipki> je pourrais faire carrière
[21:40:48] <tetue> yeh, je suis toujours là dans les réunions irc, comme une petite souris :)
[21:40:48] <franckpaul> je vais en causer à Noé, il va t'engager dans sa troupe ;-)
[21:41:03] <franckpaul> nikrou: attends un peu,
[21:41:11] <lipki> mais en fait y'a pas de point 5
[21:41:13] <franckpaul> c'est qui que tu traites de cloche ?
[21:41:28] <franckpaul> t'es tout seul dans ton tee-shirt ?
[21:41:38] <franckpaul> t'var ta jeule à la récré
[21:41:52] <Dsls> C'est comme dans Gaston, y'a pas de n°5
[21:41:57] <lipki> Même le point 5 c'était un mensonge
[21:42:10] <franckpaul> comme les 13e étages et les rez-de-chaussée aux states
[21:43:04] <lipki> llu_: Je crois que c'est le bon moment pour leurs annoncer que le CR n'existe pas
[21:43:56] <kozlika> mouarf
[21:44:01] <llu_> lipki : non je crois qu'il faut attendre encore un peu 
[21:44:41] <Dsls> 'va falloir que je teste dc sous hhvm
[21:44:42] <franckpaul> bon, tout le monde a du job pour la prochaine fois, on remet un filet de gaz et on recause de tout ça lundi ?
[21:45:15] <JcDenis> franckpaul tu la sors quand la .2 ?
[21:45:25] <franckpaul> nikrou: tu voulais pas faire un tuto sur hg vu d'un client git ?
[21:45:31] <Dsls> JcDenis: quand tu auras traité ton ticket :P
[21:45:35] <JcDenis> huhu
[21:45:38] <franckpaul> JcDenis: vendredi ou samedi
[21:45:43] <JcDenis> ok
[21:46:00] <franckpaul> spa à un ou deux jours près non plus, la 2.6.1 fonctionne bien
[21:46:10] <JcDenis> (j'ai un pc vierge donc j'ai un peu de taf a tout remettre...)
[21:46:20] <Dsls> z'avez pu éprouver les modifs sur les traitements de commentaires (cf. les pbs sqlite) ?
[21:46:38] <franckpaul> perso pas du tout
[21:47:01] <franckpaul> 'soir Tomeko 
[21:47:11] <Tomeko> bonsoir
[21:47:13] <Dsls> j'ai testé chez moi, mais comme en général, il vaut mieux que ce ne soit pas le développeur qui teste ses modifs :)
[21:47:25] <Tomeko> désolé pour le retard, gavait les voeux du maire de ma commune :)
[21:47:32] <Dsls> pour info, ça tourne bien sur du pgsql (ma prod pointe sur la branche default)
[21:47:40] <franckpaul> ok
[21:47:55] <Dsls> et j'ai testé en local sur du mysql et du sqlite
[21:47:56] <lipki> Dsls: vaudra que je pense a m'en resservir de celle là.
[21:48:32] <nikrou> @franck : oui je veux bien. Sous forme de billet ?
[21:48:32] <Dsls> btw, dc tourne très bien sur php 5.5.7
[21:48:39] <franckpaul> cool !
[21:48:47] <kozlika> JcDenis: ton install de test est à jour ? on pourrait demander aux copains de tester sur la ml
[21:48:57] <JcDenis> kozlika nop
[21:49:10] <JcDenis> je fais dans la semaine (je bricole chez moi demain avant
[21:49:12] <kozlika> oki
[21:49:31] <JcDenis> kozlika je peux virer les bricolage special gautier ou armony je sais plus 
[21:49:44] <JcDenis> sinon je vais mettre trois plombe
[21:49:48] <JcDenis> s
[21:49:54] <kozlika> oui tu peux, fais un zip avant okazou
[21:50:20] <JcDenis> oki
[21:50:23] <JcDenis> thx
[21:50:49] <franckpaul> lipki: t'as avancé sur ton idée d'attribut syntaxe sur tpl:xxx ?
[21:51:43] <lipki> je suis dessus, en fait, dans l'idéal, il faudrait réécrire deux méthode, et ajouter un behavior
[21:52:07] <franckpaul> deux méthodes ?
[21:52:32] <lipki> getFilters, avant le cache des tpl
[21:52:41] <franckpaul> ah ui
[21:52:45] <lipki> et context::global_filter dans le cache
[21:53:07] <lipki> ils gère une série de paramètre limité
[21:53:17] <lipki> par exemple
[21:53:32] <lipki> global_filter($str, $encode_xml, $remove_html, $cut_string, $lower_case, $upper_case ,$tag='')
[21:53:41] <lipki> on doit pouvoir faire mieux que ça
[21:53:49] <franckpaul> yep je comprends ce que tu veux dire
[21:54:27] <lipki> tu coup la je regarde si je peut surcharger les classes.
[21:54:38] <franckpaul> pour les autres : lipki proposait de gérer un attribut particulier du genre syntaxe="markdown" pour convertir le contenu de la balise à l'aide de la syntaxe
[21:54:40] <lipki> une autre idée ?
[21:54:58] <franckpaul> ça permettrait de mettre du MD dans un titre, par exemple et de le convertir à l'affichage
[21:55:13] <lipki> Mais ce qui serait bien c'est d'offrir la possibilité au plugin d'ajouter leurs propre filtre
[21:55:30] <Dsls> avec twig ce sera en natif
[21:55:35] Action: Dsls sifflote
[21:55:39] <franckpaul> :-p
[21:55:41] <lipki> c'est ce que j'ai penser
[21:56:30] <lipki> j'ai toujours le temps de tester le proofofconcept :)
[21:56:40] <lipki> pis ça me fait remettre les mains dans le code
[21:56:46] <franckpaul> yep, c'est intéressant 
[21:56:52] <franckpaul> cool
[21:57:09] <Dsls> ça me fait penser que j'ai aussi le PoC composer à faire :)
[21:58:00] <lipki> c'est quoi ?
[21:58:08] <franckpaul> un gestionnaire de dépendances
[21:58:14] <Dsls> passer CB et twig dans composer
[21:58:21] <Dsls> et ne plus les mettre en sous-projet
[21:58:25] <Dsls> (dans hg)
[21:59:33] <lipki> a oui je vois
[21:59:45] <franckpaul> du coup ça va impacter la génération de release, a priori
[21:59:58] <Dsls> oui
[22:00:10] <Dsls> c'est pour ça que je vois pas ça avant la 2.8
[22:00:16] <franckpaul> ok
[22:00:37] <Dsls> (sauf bonne surprise, mais je préfère être pessimiste)
[22:01:00] <Dsls> comme dirait un collègue : il vaut mieux être pessimiste, comme ça on ne peut avoir que des bonnes surprises :)
[22:01:24] <lipki> Le plan diabolique de DSLS ce m'est en place, bientôt on ne pourra plus faire demi-tour
[22:01:41] <Dsls> gniark
[22:01:55] <lipki> 5 ans de travaille acharné quand même
[22:02:04] <Dsls> mais pour ça il faudrait que mon chef me donne moins de taf
[22:02:11] <Dsls> j'avancerais plus vite sur DC
[22:02:40] <franckpaul> tu veux que je te fasse un mot d'excuse pour ton boss Dsls ?
[22:02:55] <Dsls> franckpaul: hmmm ... pas sûr qu'il soit réceptif
[22:03:09] <Dsls> là je suis en train de me battre pour obtenir rapidement 2 serveurs Oracle t5-8
[22:03:13] <Dsls> à 120k€ pièce
[22:03:38] <franckpaul> et dire que les miens tiquent quand je leur cause d'un mac mini à 1,5k¬
[22:03:49] <Dsls> :)
[22:04:08] <Dsls> et encore, c'est sans stockage
[22:04:18] <Dsls> il faut 2 baies EMC à coté :)
[22:04:21] <franckpaul> cela dit, ça fait assez lego, 1 mac mini = 1 fn serveur, alors j'empile :-)
[22:05:04] <Dsls> tout ça pour y mettre du SAP, l'horreur quoi...
[22:05:18] <franckpaul> mazette !
[22:05:19] <lipki> Vous nêtes pas un plaindre celui qui gère ma comptabilité et un psychopathe
[22:05:25] <franckpaul> ============== FIN ==============
[22:05:26] <lipki> C'est moi
[22:08:50] <lipki> kozlika
[22:08:54] <JcDenis> café!
[22:08:55] <lipki> j'ai fait un thème
[22:08:56] <kozlika> vi ?
[22:09:02] <kozlika> ahué ? cool !
[22:09:08] <lipki> bon un tout petit
[22:09:15] <lipki> un bébé thème
[22:09:26] <kozlika> tu me "remonteras" les éventuels soucis ou améliorations ?
[22:09:32] <lipki> il n'a pas encore c'est dent, c'est a pein s'il ouvre les yeux
[22:09:36] <kozlika> :-)
[22:10:14] <lipki> J'ai fait un thème currywurst qui me sert de parent, en 2.6
[22:10:20] <lipki> c'est pratique
[22:10:48] <lipki> j'ai juste une question
[22:10:57] <kozlika> oui
[22:11:09] <lipki> suspense
[22:11:13] <kozlika> :-D
[22:11:24] <lipki> a oui
[22:11:37] <lipki> tu a mis simple menu dans le header
[22:11:42] <kozlika> yep
[22:12:00] <lipki> mais tu la mis tout seul, sans une zone de widget pour lui tenir chaud
[22:12:11] <kozlika> ben vous n'avez rien codé pour ça
[22:12:26] <lipki> je trouve ça dommage, ça veut dire que l'on doit modifier le thème pour changer de plugin de mnu
[22:13:31] <kozlika> ah oui je compredns
[22:14:31] <lipki> sinon rien ne ma gênée, c'est beaucoup plus agréable comme c'est actuellement.
[22:14:32] <lipki> les includes sont très bien fait.
[22:19:12] <kozlika> \o/
[22:19:39] <kozlika> tu me feras une proposition de code pour cette histoire de widget ?
[22:19:58] <kozlika> Je pourrais peut être verser le truc sur un dépôt Mercurial pour que vous puissiez jouer ? ou Git ?
[22:20:04] <lipki> je sait pas trop , on n'a pas beaucoup de volet
[22:24:14] <kozlika> on se le garde dans un coin de la tête alors
[22:24:17] <lipki> Je sait ce qui me dérange en fait
[22:24:24] <kozlika> oui ?
[22:24:28] <lipki> le set de template est sensé être neutre
[22:25:03] <kozlika> il ne l'est peut être pas complètement, j'ai commencé par copier coller ceux de ductile puis le les "nettoie"
[22:25:04] <lipki> mais mettre un menu de navigation isolé, donc non modifiable par l'interface, c'est déjà faire un choix
[22:25:14] <kozlika> un menu de nav isolé ?
[22:26:16] <lipki> dans le sens que c'est directement la navigation, et pas une zone personalisable dans lequel on pourrais mettre de la navigation, ou des patates
[22:26:30] <kozlika> je ne compr"ends pas
[22:26:43] <franckpaul> j'suis pas trop d'accord avec "faut que le set soit neutre"
[22:27:06] <franckpaul> parce qu'on peut imaginer d'avoir plusieurs sets différents (on en aura déjà 2 avec la 2.7)
[22:27:08] <lipki> j'explique ce qui me gêne, j'ai pas forcément raison
[22:27:39] <franckpaul> et il reste à imaginer une installation de jeux de templates supplémentaires à la manière des plugins, par exemple
[22:27:50] <franckpaul> c'est un exemple
[22:28:29] <lipki> oui il peut y en avoir plusieurs, mais dans le lot, il devrait (je pense qu'il devrait) y en avoir un neutre
[22:28:32] <franckpaul> à l'inverse, mustek, le jeu de template original n'avait pas de place prévue pour un menu, il n'était donc pas neutre
[22:28:41] <lipki> comme l'était le précédent.
[22:28:46] <franckpaul> en fait tout dépend de ce qu'on entend par neutre
[22:28:52] <lipki> voilà
[22:29:06] <kozlika> simple menu est dans la distrib
[22:29:16] <lipki> c'est pas ce que je veux dire
[22:29:23] <kozlika> c'est logique de le prendre en compte (enfin dans ma logique en tout cas)
[22:30:13] <lipki> l'ancien set était conçu pour que sans css, les contenu soit organiser dans un ordre logique, et agréable pour un lecteur d'écran
[22:30:32] <lipki> avec le contenu en premier, ou le plus premier possible
[22:30:51] <lipki> et un prelude comme premier élément de navigation
[22:31:10] <kozlika> vi
[22:31:28] <lipki> donc prelude très court pour donner le choix, titre, contenu , le reste accessible par le prélude
[22:31:57] <kozlika> je comprends
[22:32:09] <lipki> volà, placer un menu dans le header, c'est choisir d'interposer un contenu qui sera relu a chaque chargement de page
[22:32:21] <lipki> avant le contenu réelle de la page
[22:33:24] <kozlika> mais ne pas le placer là créerait un problème dans l'ordre de lecture "visuel" et "oral"
[22:33:32] <kozlika> une disparité je veux dire
[22:33:37] <kozlika> idem pour la tabulation
[22:33:43] <lipki> Je pense pas
[22:33:56] <lipki> enfin je retire le je
[22:34:12] <kozlika> ?
[22:34:56] <lipki> les gans qui on bosser le problème quand les tableau on disparu de nos page html, (alors que je n'était qu'un jeune padawan) ne l'on apparament pas vu de cette manière
[22:35:19] <kozlika> uh ?
[22:35:44] <lipki> il ni a rien dans les top menu aujourd'hui qu'il ni avait hier
[22:36:01] <lipki> hors hier, les expert on dit, les menus, c'est après le contenu
[22:36:38] <kozlika> euh seulement dans les bonnes maisons
[22:36:50] <kozlika> soit 5% des sites mondiaux (au mieux)
[22:36:59] <kozlika> mais je comprends l'idée :)
[22:37:01] <lipki> plus tout les dotclear
[22:37:14] <lipki> Donc 5, 001%
[22:37:25] <lipki> :)
[22:37:29] <kozlika> Cela dit qu'est-ce qu'un menu ? autant pour le sidebar je suis d'accord, autant ce menu-là est pour moi un groupe de liens de nav rapide
[22:37:46] <lipki> un prelude donc
[22:37:47] <kozlika> et un grooooos paquet de sites tout à fait respectueux des normes le place avant le contenu
[22:38:32] <franckpaul> lipki: pas les dotclear qui utilisent Fallseason comme thème par exemple, le menu est avant le contenu
[22:38:39] <lipki> l'idée est simple, tout ce qui est avant le contenu est lu par les lecteurs d'écran, donc répéter a chaque changement de page, donc moins y'en a mieux c'est 
[22:38:45] <lipki> d'ou le prelude
[22:39:23] <kozlika> oui je sais (un peu ;-))
[22:39:27] <franckpaul> ou dcbootstrap (pourtant récent) qui met aussi le menu à l'avant
[22:39:46] <lipki> franckpaul : j'ai perdu beaucoup de cheveux a respecter cette régle, et j'en ais sauvé beaucoup en l'abandonnant souvent :)
[22:39:53] <franckpaul> :-)
[22:40:17] <franckpaul> les règles c'est bien, mais faut pas en faire une absolue contrainte non plus, je pense
[22:40:33] <Tomeko> +1
[22:40:37] <lipki> je sait bien, pour une fois que je me fait l'avocat de a11y :D
[22:41:00] <lipki> Tu dit ça parceque tu n'utilise pas de lecteur d'écran :)
[22:41:32] <Tomeko> kozlika: rien à voir, mais j'ai remarqué que seul le gabarit archive.html n'avait pas l'inclusion-facile _sidebar.html. C'est voulu ?
[22:41:51] <kozlika> non c'est parce que je ne l'ai pas encore modifié
[22:41:54] <kozlika> ni la 404
[22:42:00] <Tomeko> (je suis en train d'éplucher les fichiers du thème Currywurst)
[22:42:17] <Tomeko> ah si
[22:42:20] <kozlika> on est en train de prendre de l'avance sur la réu de demain là ;-)
[22:42:29] <Tomeko> oups
[22:42:33] <lipki> on chauffe la place
[22:42:36] <kozlika> :-D
[22:42:43] <Tomeko> ben tu le dis si tu veux pas qu'on s'intéresse !
[22:42:49] <Tomeko> :P
[22:47:33] <franckpaul> bon, 'soir les gens, c'était cool de vous avoir tous là ce soir
[22:47:35] <franckpaul> à bientôt
[22:47:48] <lipki> bonne nuit
[22:47:53] <Tomeko> à+ franckpaul 
[22:48:18] <Tomeko> ah pendant qu'il reste du monde, on part sur le 22 mars pour le prochain atelier ?
[22:48:33] <Tomeko> ceux qui n'ont pas encore répondu ?
[22:51:30] <lipki> trop tard :)
[22:52:04] <lipki> Bon c'est pas tout ça mais j'ai un cast sur le feu
[22:55:27] <mEga|work> c'est fini ou je peux dire des bêtises ? :p
[23:30:46] <llu_> bonne nuit :)