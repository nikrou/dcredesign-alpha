function chainHandler(obj, handlerName, handler) {
	obj[handlerName] = (function(existingFunction) {
		return function() {
			handler.apply(this, arguments);
			if (existingFunction)
				existingFunction.apply(this, arguments); 
		};
	})(handlerName in obj ? obj[handlerName] : null);
};

chainHandler(window,'onload',function() {
	if (document.getElementById) {
		var e = document.getElementById('auth_login');
		if (e) { e.onfocus = function() { this.value = '';} }
		e = document.getElementById('auth_passw');
		if (e) { e.onfocus = function() { this.value = '';} }
	}
});