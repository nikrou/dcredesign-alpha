$(function() {
	var tab = null;
	if (document.location.hash) {
		tab = document.location.hash.substr(1);
		if ($('#' + tab).length == 0) {
			tab = null;
		}
	}
	$.pageTabs(tab);
	$(window).scrollTop(0);
	
	$('#screenshots a[href$=.jpg]').modalImages({
		loader_img: 'http://www.dotclear.net/js/modal/loader.gif',
		close_img: 'http://www.dotclear.net/js/modal/close.png',
		prev_img: 'http://www.dotclear.net/js/modal/prev.png',
		next_img: 'http://www.dotclear.net/js/modal/next.png',
		blank_img: 'http://www.dotclear.net/js/modal/blank.gif'
	});
});
