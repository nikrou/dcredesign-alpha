$(function() {
	var tab = null;
	if (document.location.hash) {
		tab = document.location.hash.substr(1);
		if ($('#' + tab).length == 0) {
			tab = null;
		}
	}
	$.pageTabs(tab);
	$(window).scrollTop(0);
});
