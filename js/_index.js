$(function() {
	// Dotclear powered sites
	var sites = {
		'fr.dotclear.org' : [
			['gandi','http://www.gandi.net/domaine/blog/','GandiBlog'],
			['rtl','http://www.rtl.fr/radio/blogs.asp','RTL'],
			['parisjob','http://blog.parisjob.com/','ParisJob'],
			['pcf','http://blogs.gauchepopulaire.fr/','Blogs Gauche Populaire'],
			['france2','http://blog.france2.fr/portal.php','France Télévision'],
			['marine','http://jdb.marine.defense.gouv.fr/','Journaux de bord de la marine nationale'],
			['01net','http://expert.01net.com/expert/','Le blog des experts 01net'],
			['sfr','http://www.sfrdeveloppement.fr/','Le Blog SFR Développement']
		],
		'dotclear.org' : [
			['liveblot','http://liveblot.com/','Liveblot'],
			['softaculous','http://www.softaculous.com/','Softaculous']
		]
	};
	var headlines = {
		'fr.dotclear.org' : '<h2>Utilisateurs</h2><p>Ils font confiance à Dotclear pour leurs blogs. <a href="/download">Pourquoi pas vous ?</a></p>',
		'dotclear.org' : '<h2>Partners</h2><p>They use Dotclear or provide a service with Dotclear included. <a href="/download">Why don\'t you?</a></p>'
	};
	
	if (sites[document.location.hostname] != undefined) {
		var a = sites[document.location.hostname];
		var d = $(document.createElement('div')).attr('id','users').append(headlines[document.location.hostname]);
		var l = $(document.createElement('ul'));
		a.sort(function() { return 0.5 - Math.random();});
		for (i = 0; i < 4 && i < a.length; i++) {
			l.append('<li><a href="' + a[i][1] + '" title="' + a[i][2] + '"><img alt="' + a[i][2] + '" src="/images/logo-users/' + a[i][0] + '.png	" /></a></li>');
		}
		$('#contrib').after(d.append(l));
	}
	
	// Language hint
	$.post('/language',{guess:1},function(data) {
		if (data) {
			$('#d-wrapper').prepend('<p id="lang-guess">' + data + '</p>');
		}
	});
});
