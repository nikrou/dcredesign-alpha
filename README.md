# dc-redesign alpha #

### Dépôt pour le redesign du site dotclear.org et ses satellites ###

* work in progress visible sur http://feignasses.org | http://fr.feignasses.org
* actuellement le travail porte sur l'accueil général et le blog

### Environnement nécessaire ###

* 1 install dotclear
* 2 blogs (un anglais, par défaut, un français)

Architecture :

* les templates servis sont dans lib/themes/new-dotclear/tpl
* les sources sass, scripts, css et images sont dans new-style/
* les sources des images et les maquettes sont dans _working-files

### Contribution guidelines ###

* Propositions par pull requests
* Guidelines dans _working-files/style guide

### À qui m'adresser ? ###

* Flafay ou Kozlika